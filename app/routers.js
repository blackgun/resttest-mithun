/**
* Initializing the Router essentials
**/
var express     = require('express'),
    Passport    = require('passport'),
    Auth        = require('./Component/Auth'), 
    Account        = require('./Controller/AccountsController'),
    app         = express();

/** Created an instance of the express Router **/
var router = express.Router(); 

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    res.setHeader('Access-Control-Allow-Headers', 'apikey');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

/** Initialize Passport.js **/
app.use(Passport.initialize());



/** ROUTER DECLARATION STARTS HERE **/

router.get('/unauthorized', function(req, res) {
  res.json({ message: "The request is Unauthorized" });
});

router.route('/')
  .get(Auth.isAuthenticated, Account.getAccounts);

/** Create endpoint handlers for /accounts */
router.route('/accounts')
  .post(Auth.isAuthenticated, Account.postAccounts)
  .get(Auth.isAuthenticated, Account.getAccounts);

router.route('/account/:id')
  .get(Auth.isAuthenticated, Account.getAccount)
  .put(Auth.isAuthenticated, Account.updateAccount)
  .delete(Auth.isAuthenticated, Account.deleteAccount);

/** Special routes */

router.route('/account/:id/activate')
  .get(Auth.isAuthenticated, Account.accountActivate)
router.route('/account/:id/deactivate')
  .get(Auth.isAuthenticated, Account.accountDeactivate)
router.route('/accounts/batch')
  .post(Auth.isAuthenticated, Account.postBulkAccounts)


/** Expose router to other modules **/
module.exports = router;